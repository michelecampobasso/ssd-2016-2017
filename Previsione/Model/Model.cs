﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using System.Collections.Generic;
using RDotNet;
using System.IO;

namespace SSD_2016_2017
{
    class Model
    {
        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento (mittente e testo generato per l'interfaccia)

        public event viewEventHandler FlushText;  // questo genera l'evento

        private REngine engine;

        private REngine SetupREngine()
        {
            REngine.SetEnvironmentVariables();
            REngine engine = REngine.GetInstance();
            engine.Initialize();

            // MURPHY IS WATCHING YOU...  ( ͡° ͜ʖ ͡°)-︻デ┳═ー

            //engine.Evaluate("install.packages(\"tseries\")");
            //engine.Evaluate("install.packages(\"forecast\")");

            engine.Evaluate("library(tseries)");
            engine.Evaluate("library(forecast)");
            return engine;
        }

        private void LoadHistoricalData()
        {
            engine.Evaluate("dataset <- read.table(file.choose(), header=TRUE, sep = ',')");
        }

        private void PrintCharacterMatrix(CharacterMatrix matrix)
        {
            for (int i = 0; i < matrix.RowCount; i++)
            {
                for (int j = 0; j < matrix.ColumnCount; j++)
                {
                    FlushText(this, matrix[i, j].ToString());
                }
                FlushText(this, "\n");
            }
        }

        public void ReadHistoricalSeriesFromCSVAndPrintSales(string seriesId)
        {
            if (engine == null)
            {
                engine = SetupREngine();
                LoadHistoricalData();
            }

            engine.Evaluate("datasetOfSeries <- subset(dataset,id_serie=="+seriesId+")");
            engine.Evaluate("myts <- ts(datasetOfSeries[, 4], start = c(0), end = c(34), frequency = 1)");

            CharacterMatrix acf = engine.Evaluate("acf(ts(diff(myts)), main = 'ACF Sales')").AsCharacterMatrix();
            PrintCharacterMatrix(acf); 

            engine.Evaluate("plot(myts, xlab = 'Months', ylab = 'Sales', col = \"red\")");
            engine.Evaluate("abline(reg = lm(myts~time(myts)))");

        }

        public void ReadHistoricalSeriesFromCSVAndPrintSARIMA(string seriesId)
        {
            if (engine == null)
            {
                engine = SetupREngine();
                LoadHistoricalData();
            }

            engine.Evaluate("datasetOfSeries <- subset(dataset,id_serie==" + seriesId + ")");
            engine.Evaluate("myts <- ts(datasetOfSeries[, 4], start = c(0), end = c(34), frequency = 1)");
            engine.Evaluate("ARIMAfit <- auto.arima(myts, stepwise = FALSE, approximation = FALSE)");

            CharacterMatrix arimaSummary = engine.Evaluate("print(summary(ARIMAfit))").AsCharacterMatrix();
            PrintCharacterMatrix(arimaSummary);

            engine.Evaluate("plot(ARIMAfit$x, col = \"black\")");
            engine.Evaluate("lines(fitted(ARIMAfit), col = \"red\")");

        }

        public void ReadHistoricalSeriesFromCSVAndPrintForecasts(string seriesId)
        {
            if (engine == null)
            {
                engine = SetupREngine();
                LoadHistoricalData();
            }

            engine.Evaluate("datasetOfSeries <- subset(dataset,id_serie=="+seriesId+")");
            engine.Evaluate("myts <- ts(datasetOfSeries[, 4], start = c(0), end = c(34), frequency = 1)");
            engine.Evaluate("ARIMAfit <- auto.arima(myts, stepwise = FALSE, approximation = FALSE)");
            CharacterMatrix arimaSummary = engine.Evaluate("print(summary(ARIMAfit))").AsCharacterMatrix();
            PrintCharacterMatrix(arimaSummary);

            engine.Evaluate("pred = forecast(ARIMAfit)");
            CharacterMatrix preds = engine.Evaluate("pred").AsCharacterMatrix();
            PrintCharacterMatrix(preds);

            engine.Evaluate("plot(myts, type = 'l', xlab = 'Months', ylab = 'Sales')");
            engine.Evaluate("plot(forecast(ARIMAfit, h = 6))");


        }

        private IDbConnection CreateConnectionThroughExplicitRequest(ConnectionStringSettings connectionStringSettings, bool isSqlLiteDb)
        {
            IDbConnection connection = null;
            if (isSqlLiteDb)
                connection = new SQLiteConnection(connectionStringSettings.ConnectionString);
            else
                connection = new SqlConnection(connectionStringSettings.ConnectionString);
            connection.Open();

            return connection;
        }

        private IDbConnection CreateConnectionThroughDBFactory(string dbFactoryName, ConnectionStringSettings connectionStringSettings)
        {
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(dbFactoryName);
            IDbConnection connection = dbFactory.CreateConnection();
            connection.ConnectionString = connectionStringSettings.ConnectionString;
            connection.Open();

            return connection;
        }

        private IDataReader QueryDataFromDB(IDbConnection connection, string query, Dictionary<string, object> paramsToBeQueried)
        {
            IDbCommand command = connection.CreateCommand();
            command.CommandText = query;

            // Adding every needed parameter in a safe way
            foreach (KeyValuePair<string, object> param in paramsToBeQueried)
            {
                IDbDataParameter safeParam = command.CreateParameter();
                // safeParam.DbType = DbType.Int32; <<<----- Removed thanks to the automatic inference of types and for the check I do in the View.
                safeParam.ParameterName = "@" + param.Key;
                safeParam.Value = param.Value;
                command.Parameters.Add(safeParam);
            }

            return command.ExecuteReader();
        }

        private void PrintDataFromReader(IDataReader reader, string[] paramsToPrint)
        {
            while (reader.Read())
            {
                // Flushing every single parameter required
                foreach (string par in paramsToPrint)
                    FlushText(this, reader[par] + "\t");
                FlushText(this, Environment.NewLine);
            }
            reader.Close();
        }

        public void ConnectAndRead(ConnectionStringSettings connectionStringSettings, bool isSqlLiteDb, string idSerie)
        {
            IDbConnection connection = null;
            try
            {
                connection = CreateConnectionThroughExplicitRequest(connectionStringSettings, isSqlLiteDb);

                var paramsToBeShown = new string[] { "periodo", "val" };
                var paramsToBeQueried = new Dictionary<string, object>();
                paramsToBeQueried["idserie"] = idSerie;

                IDataReader reader = QueryDataFromDB(connection, "select h.periodo, h.val from histordini as h WHERE idserie=" + idSerie, paramsToBeQueried);
                PrintDataFromReader(reader, paramsToBeShown);

            }
            catch (Exception e)
            {
                FlushText(this, "[FillDataSet] Error: " + e.Message + Environment.NewLine);
            }
            finally
            {
                if (connection != null)
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
            }
        }

        public void ConnectWithDataSetAndProviderFactoryAndRead(ConnectionStringSettings connectionStringSettings, string dbFactoryName, string idSerie)
        {
            IDbConnection connection = null;
            try
            {
                connection = CreateConnectionThroughDBFactory(dbFactoryName, connectionStringSettings);

                var paramsToBeShown = new string[] { "periodo", "val" };
                var paramsToBeQueried = new Dictionary<string, object>();
                paramsToBeQueried["idserie"] = idSerie;

                IDataReader reader = QueryDataFromDB(connection, "select h.periodo, h.val from histordini as h WHERE idserie=" + idSerie, paramsToBeQueried);
                PrintDataFromReader(reader, paramsToBeShown);
            }
            catch (Exception ex)
            {
                FlushText(this, "[FillDataSet] Error: " + ex.Message + Environment.NewLine);
            }
            finally
            {
                if (connection != null)
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
            }
        }


        public void ConnectAndReadViaEF(string idSerie)
        {
            using (var context = new ordini2017Entities())
            {
                IDbConnection connection = context.Database.Connection;
                connection.Open();
                try
                {
                    var paramsToBeShown = new string[] { "periodo", "val" };
                    var paramsToBeQueried = new Dictionary<string, object>();
                    paramsToBeQueried["idserie"] = idSerie;

                    IDataReader reader = QueryDataFromDB(connection, "select h.periodo, h.val from histordini as h WHERE idserie=" + idSerie, paramsToBeQueried);
                    PrintDataFromReader(reader, paramsToBeShown);
                }
                catch (Exception ex)
                {
                    FlushText(this, "[FillDataSet] Error: " + ex.Message + Environment.NewLine);
                }
                finally
                {
                    if (connection != null)
                        if (connection.State == ConnectionState.Open)
                            connection.Close();
                }

                /* List<histordini> selectedOrders = new List<histordini>();
                foreach (histordini o in selectedHistory)
                {
                    selectedOrders.Add(o);
                }*/

                // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv 
                // FlushText(this, JsonConvert.SerializeObject(myOrdini)); <<<<------- Serializzatore JSon per WebAPI <<<<<<<<<<<<<<<<<<<<<<<
                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            }
        }
    }
}

