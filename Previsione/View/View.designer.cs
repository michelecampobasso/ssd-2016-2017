﻿using System.Windows.Forms;

namespace SSD_2016_2017
{
    partial class View : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.defaultStart = new System.Windows.Forms.Button();
            this.textOutput = new System.Windows.Forms.TextBox();
            this.rbSQLite = new System.Windows.Forms.RadioButton();
            this.rbSQLServer = new System.Windows.Forms.RadioButton();
            this.gbServer = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.historicSeriesBox = new System.Windows.Forms.TextBox();
            this.startWithPF = new System.Windows.Forms.Button();
            this.startWithEF = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.gbServer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // defaultStart
            // 
            this.defaultStart.Location = new System.Drawing.Point(13, 162);
            this.defaultStart.Margin = new System.Windows.Forms.Padding(4);
            this.defaultStart.Name = "defaultStart";
            this.defaultStart.Size = new System.Drawing.Size(74, 44);
            this.defaultStart.TabIndex = 0;
            this.defaultStart.Text = "START";
            this.defaultStart.UseVisualStyleBackColor = true;
            this.defaultStart.Click += new System.EventHandler(this.DefaultConnect);
            // 
            // textOutput
            // 
            this.textOutput.Location = new System.Drawing.Point(224, 14);
            this.textOutput.Margin = new System.Windows.Forms.Padding(4);
            this.textOutput.Multiline = true;
            this.textOutput.Name = "textOutput";
            this.textOutput.ReadOnly = true;
            this.textOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textOutput.Size = new System.Drawing.Size(420, 352);
            this.textOutput.TabIndex = 1;
            // 
            // rbSQLite
            // 
            this.rbSQLite.AutoSize = true;
            this.rbSQLite.Location = new System.Drawing.Point(8, 23);
            this.rbSQLite.Margin = new System.Windows.Forms.Padding(4);
            this.rbSQLite.Name = "rbSQLite";
            this.rbSQLite.Size = new System.Drawing.Size(80, 21);
            this.rbSQLite.TabIndex = 3;
            this.rbSQLite.TabStop = true;
            this.rbSQLite.Text = "SQLLite";
            this.rbSQLite.UseVisualStyleBackColor = true;
            this.rbSQLite.CheckedChanged += new System.EventHandler(this.rbSQLite_CheckedChanged);
            // 
            // rbSQLServer
            // 
            this.rbSQLServer.AutoSize = true;
            this.rbSQLServer.Location = new System.Drawing.Point(96, 23);
            this.rbSQLServer.Margin = new System.Windows.Forms.Padding(4);
            this.rbSQLServer.Name = "rbSQLServer";
            this.rbSQLServer.Size = new System.Drawing.Size(103, 21);
            this.rbSQLServer.TabIndex = 4;
            this.rbSQLServer.TabStop = true;
            this.rbSQLServer.Text = "SQL Server";
            this.rbSQLServer.UseVisualStyleBackColor = true;
            // 
            // gbServer
            // 
            this.gbServer.Controls.Add(this.rbSQLite);
            this.gbServer.Controls.Add(this.rbSQLServer);
            this.gbServer.Location = new System.Drawing.Point(12, 61);
            this.gbServer.Margin = new System.Windows.Forms.Padding(4);
            this.gbServer.Name = "gbServer";
            this.gbServer.Padding = new System.Windows.Forms.Padding(4);
            this.gbServer.Size = new System.Drawing.Size(204, 57);
            this.gbServer.TabIndex = 5;
            this.gbServer.TabStop = false;
            this.gbServer.Text = "DBMS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Id historic series";
            // 
            // historicSeriesBox
            // 
            this.historicSeriesBox.Location = new System.Drawing.Point(133, 125);
            this.historicSeriesBox.Name = "historicSeriesBox";
            this.historicSeriesBox.Size = new System.Drawing.Size(64, 22);
            this.historicSeriesBox.TabIndex = 7;
            // 
            // startWithPF
            // 
            this.startWithPF.Location = new System.Drawing.Point(12, 213);
            this.startWithPF.Name = "startWithPF";
            this.startWithPF.Size = new System.Drawing.Size(204, 45);
            this.startWithPF.TabIndex = 8;
            this.startWithPF.Text = "START WITH PROVIDER FACTORY";
            this.startWithPF.UseVisualStyleBackColor = true;
            this.startWithPF.Click += new System.EventHandler(this.ConnectWithDataSetAndProviderFactoryAndRead);
            // 
            // startWithEF
            // 
            this.startWithEF.Location = new System.Drawing.Point(94, 162);
            this.startWithEF.Name = "startWithEF";
            this.startWithEF.Size = new System.Drawing.Size(122, 44);
            this.startWithEF.TabIndex = 9;
            this.startWithEF.Text = "START WITH EF";
            this.startWithEF.UseVisualStyleBackColor = true;
            this.startWithEF.Click += new System.EventHandler(this.ConnectAndReadViaEF);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(652, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 351);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(44, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Historical data view";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = " from different sources";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(92, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Forecast future sales";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(225, 82);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(64, 22);
            this.textBox1.TabIndex = 9;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Id historic series";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(48, 129);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 51);
            this.button1.TabIndex = 10;
            this.button1.Text = "SHOW HISTORICAL DATA AND TREND";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(204, 129);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 51);
            this.button2.TabIndex = 11;
            this.button2.Text = "SHOW SARIMA AND ORIGINAL DATA";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(96, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(200, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = " from historical data";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(126, 187);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 51);
            this.button3.TabIndex = 13;
            this.button3.Text = "MAKE FORECASTS";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 405);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.startWithEF);
            this.Controls.Add(this.startWithPF);
            this.Controls.Add(this.historicSeriesBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbServer);
            this.Controls.Add(this.textOutput);
            this.Controls.Add(this.defaultStart);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "View";
            this.Text = "Historical data viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbServer.ResumeLayout(false);
            this.gbServer.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button defaultStart;
        private System.Windows.Forms.TextBox textOutput;
        private System.Windows.Forms.RadioButton rbSQLite;
        private System.Windows.Forms.RadioButton rbSQLServer;
        private System.Windows.Forms.GroupBox gbServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox historicSeriesBox;
        private System.Windows.Forms.Button startWithPF;
        private System.Windows.Forms.Button startWithEF;
        private Panel panel1;
        private Label label2;
        private Label label4;
        private Label label3;
        private Button button1;
        private TextBox textBox1;
        private Label label5;
        private Button button2;
        private Label label6;
        private Button button3;
    }
}

