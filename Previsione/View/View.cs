﻿using System;
using System.Windows.Forms;

namespace SSD_2016_2017
{
    public partial class View : Form
    {
        Controller myController = new Controller();

        public View()
        {
            InitializeComponent();
            myController.FlushText += ViewEventHandler; // associo il codice all'handler nella applogic
        }

        private void ViewEventHandler(object sender, string textToWrite)
        {
            textOutput.AppendText(textToWrite);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void rbSQLite_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void DefaultConnect(object sender, EventArgs e)
        {
            if (ValidateDBQuery())
                myController.Connect(rbSQLite.Checked, historicSeriesBox.Text);
        }

        private void ConnectAndReadViaEF(object sender, EventArgs e)
        {
            if (ValidateDBQuery())
                myController.ConnectAndReadViaEF(rbSQLite.Checked, historicSeriesBox.Text);
        }

        private void ConnectWithDataSetAndProviderFactoryAndRead(object sender, EventArgs e)
        {
            if (ValidateDBQuery())
                myController.ConnectWithDataSetAndProviderFactoryAndRead(rbSQLite.Checked, historicSeriesBox.Text);
        }

        private bool ValidateDBQuery()
        {
            int res; // needed for int.TryParse but never used...
            bool valid = (rbSQLite.Checked || rbSQLServer.Checked) && int.TryParse(historicSeriesBox.Text, out res);
            if (!valid)
                textOutput.AppendText("Wrong parameters!\n");
            return valid;
        }

        private bool ValidateRQuery()
        {
            int res; // same as above.
            bool valid = (int.TryParse(textBox1.Text, out res));
            if (!valid)
                textOutput.AppendText("Wrong parameters!\n");
            return valid;
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ValidateRQuery())
                myController.QueryRAndPrintSales(textBox1.Text);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ValidateRQuery())
                myController.QueryRAndPrintSARIMA(textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ValidateRQuery())
                myController.QueryRAndPrintForecasts(textBox1.Text);
        }
    }
}
