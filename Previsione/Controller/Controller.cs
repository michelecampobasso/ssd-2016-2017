﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;

namespace SSD_2016_2017
{
    class Controller
    {
        Model myModel = new Model(); 

        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento
        public event viewEventHandler FlushText;  // questo genera l'evento

        public Controller()
        {
            myModel.FlushText += controllerViewEventHandler; //Aggiungo un handler agli eventi del model 
        }

        private void controllerViewEventHandler(object sender, string textToWrite)
        {
            FlushText(this, textToWrite);
        }

        public void QueryRAndPrintSales(string idSerie)
        {
            myModel.ReadHistoricalSeriesFromCSVAndPrintSales(idSerie);
        }

        public void QueryRAndPrintSARIMA(string idSerie)
        {
            myModel.ReadHistoricalSeriesFromCSVAndPrintSARIMA(idSerie);
        }

        public void QueryRAndPrintForecasts(string idSerie)
        {
            myModel.ReadHistoricalSeriesFromCSVAndPrintForecasts(idSerie);
        }

        public void Connect(bool isSqlLite, string idSerie)
        {
            ConnectionStringSettings sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"];
            ConnectionStringSettings sqLServerConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"];

            myModel.ConnectAndRead(isSqlLite ? sqLiteConnString : sqLServerConnString, isSqlLite, idSerie);
        }

        public void ConnectWithDataSetAndProviderFactoryAndRead(bool isSqlLite, string idCliente)
        {
            ConnectionStringSettings sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"];
            ConnectionStringSettings sqlServerConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"];
            string dbFactorySqLite = "System.Data.SQLite";
            string dbFactorySqlServer = "System.Data.SqlClient";

            myModel.ConnectWithDataSetAndProviderFactoryAndRead(isSqlLite ? sqLiteConnString : sqlServerConnString, isSqlLite ? dbFactorySqLite : dbFactorySqlServer, idCliente);
        }

        public void ConnectAndReadViaEF(bool isSqlLite, string idCliente)
        {
            if (isSqlLite)
                FlushText(this, "Database not supported.");
            else
                myModel.ConnectAndReadViaEF(idCliente);
        }
    }
}
